export default async function main() {
  const data = await fetch("./data.json").then(result => result.json());
  console.log(data.name);

  let index = -1;
  let node = document.getElementById("card");

  function nextCard() {
    index++;
    node.innerHTML = createCard(data.cards[index]);
  }
  
  function backCard() {
    index--;
    node.innerHTML = createCard(data.cards[index]);
  }
  
  nextCard();

  // https://stackoverflow.com/a/44213036
  window.addEventListener("keydown", function(event) {
    switch (event.key) {
      case "ArrowLeft":
        console.log("Back!");
        backCard();
        break;
      case "ArrowRight":
        console.log("Next!");
        nextCard();
        break;
      case "ArrowUp":
        break;
      case "ArrowDown":
        break;
      case "ArrowDown":
        break;
      case "f":
        index=17;
        console.log("Question!");
        nextCard();
        break;
      case "s":
        index=-1;
        console.log("Start!");
        nextCard();
        break;
      default:
        console.log("No action för key");
    }
  });
}

function createCard(card) {
  console.log(card.name);
  
  let result = "";
  
  if(card.image) {
	  result+=`<img src="${card.image}" alt="${card.name}" >\n`;
  }
 
  result+=`<div class="content">`;
  
  if(card.title) {
	  result+=`<h1> ${card.title} </h1>" >\n`;
  }
  
  if(card.description) {
	  result+=`<p> ${card.description} </p>\n`;
  }
  
  if(card.statement) {
	  result+=`<ol>\n`;
	  for(const item of card.statement) {
	      result+=`<li> ${item} </li>\n`;  
	  }
	  result+=`</ol>\n`;
  }
  
  result+=`</div>\n`
  
  return result;
}
